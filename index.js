var app = require('express')();     //import express
var logic = require('./logic');     //เรียกใช้ไฟล์ logic จากตำแหน่ง ./logic
var bodyParser = require('body-parser')

var port = process.env.PORT || 7777;    //port ที่เปิด api เก็บค่าไว้ใน port

app.get('/', function (req, res) {      //get 
	res.send('<h1>Hello Node.js</h1>');
});
// GET
app.get('/logic1/:n' , function (req, res) {      //ประกาศว่าตัวต่อจาก / จะเป็นค่าที่รับมา มีมากกว่า 1 ให้่ใส่ / (เรียงข้อมูล)       
    var result = new logic().logic1(req.params.n)       //รับค่าผ่าน url get => req.params.X
    res.json(result)
    console.log(result)
    console.log(req.params.n)
});
// POST
app.post(`/logic2` , function (req , res){
    var body = req.body                     //รับค่าจาก post ถ้าะเจาะจงข้อมูลให้ body.X เข้าไป
    var result = new logic().logic2(body)
    res.json(result)
    console.log(result)
});

app.listen(port, function() {       //เรียกใช้ตัว port เป็นการจอง port 7777
	console.log('Starting node.js on port ' + port);
})