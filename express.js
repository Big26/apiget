var app = require('express')();     //import express
var logic = require('./logic');     //เรียกใช้ไฟล์ logic จากตำแหน่ง ./logic
var bodyParser = require('body-parser')

module.exports = function(){
    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({ extended: true }))
    return app
}